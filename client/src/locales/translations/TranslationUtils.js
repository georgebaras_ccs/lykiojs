import i18n from '../../i18n';

const changeLanguage = (lng) => {
    localStorage.setItem("i18n", lng)
    i18n.changeLanguage(lng);
}

export default changeLanguage;