import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {
  FRONTEND_REGISTER_URL,
  FRONTEND_LOGIN_URL,
  FRONTEND_FORGOTPASS_URL,
  FRONTEND_RESENDACTIVATION_URL,
  FRONTEND_PASSWORDRESET_URL,
  FRONTEND_VERIFYACCOUNT_URL,
  HOME_URL,
  STORIES_URL,
  CONTENT_MANAGEMENT_URL,
  CONTENT_MANAGEMENT_CREATE_URL,
  CONTENT_MANAGEMENT_MODERATE_URL,
  USER_MANAGEMENT_URL,
  CREATE_URL,
  GUIDES_URL,
  IDEOLOGY_URL,
  SETTINGS_URL,
  SEARCH_URL,
  REPORTS_URL,
  PROFILE_URL,
  NOTIFICATIONS_URL,
  MESSAGES_URL,
  LIBRARY_URL,
  LEADERBOARDS_URL} from './Utils/Constants';

import RegisterScreen from './components/screens/auth/RegisterScreen';
import LoginScreen from './components/screens/auth/LoginScreen';
import ForgotPaswordScreen from './components/screens/auth/ForgotPaswordScreen';
import ResetPaswordScreen from './components/screens/auth/ResetPaswordScreen';
import VerifyAccountScreen  from './components/screens/auth/VerifyAccountScreen';
import ResendActivationScreen from './components/screens/auth/ResendActivationScreen';

import LykioAuthorizedRoute from './components/routing/LykioAuthorizedRoute';
import Home from './components/screens/home/Home';
import Stories from './components/screens/stories/Stories';
import ContentManagement from './components/screens/contentmanagement/ContentManagement';
import ContentCreation from './components/screens/contentmanagement/ContentCreation';
import ContentModeration from './components/screens/contentmanagement/ContentModeration';
import UserManagement from './components/screens/usermanagement/UserManagement';
import Create from './components/screens/create/Create';
import Guides from './components/screens/guides/Guides';
import Ideology from './components/screens/ideology/Ideology';
import Settings from './components/screens/settings/Settings';
import Search from './components/screens/search/Search';
import Reports from './components/screens/reports/Reports';
import Profile from './components/screens/profile/Profile';
import Notifications from './components/screens/notifications/Notifications';
import Messages from './components/screens/messages/Messages';
import Library from './components/screens/library/Library';
import Leaderboards from './components/screens/leaderboards/Leaderboards';

const App = () => {
  return (
    <Router>
      <div className="app">
        <Switch>
          <Route exact path={FRONTEND_REGISTER_URL} component={RegisterScreen}/>
          <Route exact path={FRONTEND_LOGIN_URL} component={LoginScreen}/>
          <Route exact path={FRONTEND_FORGOTPASS_URL} component={ForgotPaswordScreen}/>
          <Route exact path={FRONTEND_RESENDACTIVATION_URL} component={ResendActivationScreen}/>
          <Route exact path={FRONTEND_PASSWORDRESET_URL} component={ResetPaswordScreen}/>
          <Route exact path={FRONTEND_VERIFYACCOUNT_URL} component={VerifyAccountScreen}/>
          <LykioAuthorizedRoute exact path={HOME_URL} component={Home}/>
          <LykioAuthorizedRoute exact path={STORIES_URL} component={Stories}/>
          <LykioAuthorizedRoute exact path={CONTENT_MANAGEMENT_URL} component={ContentManagement}/>
          <LykioAuthorizedRoute exact path={CONTENT_MANAGEMENT_CREATE_URL} component={ContentCreation}/>
          <LykioAuthorizedRoute exact path={CONTENT_MANAGEMENT_MODERATE_URL} component={ContentModeration}/>
          <LykioAuthorizedRoute exact path={USER_MANAGEMENT_URL} component={UserManagement}/>
          <LykioAuthorizedRoute exact path={CREATE_URL} component={Create}/>
          <LykioAuthorizedRoute exact path={GUIDES_URL} component={Guides}/>
          <LykioAuthorizedRoute exact path={IDEOLOGY_URL} component={Ideology}/>
          <LykioAuthorizedRoute exact path={SETTINGS_URL} component={Settings}/>
          <LykioAuthorizedRoute exact path={SEARCH_URL} component={Search}/>
          <LykioAuthorizedRoute exact path={REPORTS_URL} component={Reports}/>
          <LykioAuthorizedRoute exact path={PROFILE_URL} component={Profile}/>
          <LykioAuthorizedRoute exact path={NOTIFICATIONS_URL} component={Notifications}/>
          <LykioAuthorizedRoute exact path={MESSAGES_URL} component={Messages}/>
          <LykioAuthorizedRoute exact path={LIBRARY_URL} component={Library}/>
          <LykioAuthorizedRoute exact path={LEADERBOARDS_URL} component={Leaderboards}/>
        </Switch>
        </div>
    </Router>
  );
}

export default App;