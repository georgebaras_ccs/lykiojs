import { useState } from 'react';
import axios from 'axios';
import Navigation from '../common/Navigation';
import {AUTHORIZED_URL_PREFIX, CREATE_URL} from '../../../Utils/Constants';
import './create.css';
import { withNamespaces } from 'react-i18next';


const Create = ({t,history}) => {
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [title, setTitle] = useState("");
  const [file, setFile] = useState();

  const storageAuthTokenValue = localStorage.getItem("authToken");
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
      Authorization: `Bearer ${storageAuthTokenValue}`,
    },
  };

  const storyUploadHandler = async (e) => {
    e.preventDefault();
    try {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("title", title);

        const {data} = await axios.post(`${AUTHORIZED_URL_PREFIX}${CREATE_URL}`, formData, config);
        if(data.success===true){
            setTitle("");
            setFile();
            setSuccess(data.data);
            setTimeout(()=>{
              setSuccess("")
            }, 5000);
        }
    } catch (error) {
        setError(error.response.data.error);
        setTimeout(()=>{
            setError("")
        }, 5000);
    }
}

  return error ? (
    <span className="error-message">{error}</span>
  ) : (
    <div>
    <Navigation></Navigation>
    <div className="create-story-screen under-navbar">
    <form onSubmit={storyUploadHandler} className="create-story-screen__form">
            <h3 className="create-story-screen__title">Create a story</h3>
            {error && <span className="error-message">{error}</span>}
            {success && <span className="success-message">{success}</span>}
            <div className="form-group">
                <label htmlFor="title">Title:</label>
                <input
                    type="text" required id="title" placeholder="Enter title" value={title}
                    onChange={(e)=> {setTitle(e.target.value)}}/>
                <label htmlFor="file">File</label>
                <input 
                    filename={file} type="file" accept="image/*" 
                    onChange={e => setFile(e.target.files[0])}/>
            </div>
            <button type="submit" className="btn btn-primary">Upload Story</button>
        </form>
      </div>
    </div>
  );
};

export default withNamespaces()(Create);