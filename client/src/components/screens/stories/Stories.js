import { useState, useEffect } from 'react';
import axios from 'axios';
import Navigation from '../common/Navigation';
import {AUTHORIZED_URL_PREFIX, STORIES_URL} from '../../../Utils/Constants';
import './stories.css';
import { withNamespaces } from 'react-i18next';
import Story from './Story';

const Stories = ({t,history}) => {
  const [error, setError] = useState("");
  const [stories, setStories] = useState([]);

  useEffect(() => {
    const storageAuthTokenValue = localStorage.getItem("authToken");
    console.log(`PrivateScreen-useEffect: found token: ${storageAuthTokenValue}`);
    if(!storageAuthTokenValue){
        window.history.push("/login")
    }

    const fetchStoriesData = async () => {
      const storageAuthTokenValue = localStorage.getItem("authToken");
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${storageAuthTokenValue}`,
        },
      };

      try {
        console.log(`PrivateScreen-before call: about to make a call to /api/private with token: ${storageAuthTokenValue}`);
        const { data } = await axios.get(`${AUTHORIZED_URL_PREFIX}${STORIES_URL}`, config);
        console.log(data);
        setStories(data.data);
      } catch (error) {
        localStorage.removeItem("authToken");
        setError("You are not authorized please login");
      }
    };

    fetchStoriesData();
  }, []);
  return error ? (
    <span className="error-message">{error}</span>
  ) : (
    <div>
    <Navigation></Navigation>
    <div className='stories-flex under-navbar'>
    {stories.map(story => {
        return <Story s3Url={story.s3Url} title={story.title}/>
    })}
    </div>
    </div>
  );
};

export default withNamespaces()(Stories);