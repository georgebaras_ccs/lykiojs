import './story.css';
import { withNamespaces } from 'react-i18next';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

const Story = (props) => {
	return (
		<Card style={{ width: '18rem' }}>
			<Card.Img variant="top" src={props.s3Url} />
			<Card.Body>
				<Card.Title>{props.title}</Card.Title>
				<Card.Text>
					Some quick example text to build on the card title and make up the bulk of the card's content.
				</Card.Text>
				<Button variant="primary">Go somewhere</Button>
			</Card.Body>
		</Card>
	);
};

export default withNamespaces()(Story);
