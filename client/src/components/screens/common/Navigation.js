import {Link, useHistory} from 'react-router-dom';
import './navigation.css';
import { withNamespaces } from 'react-i18next';
import lykio_logo from '../../../multimedia/lykio_logo_transparent.png';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';


const Navigation = ({t}) => {
    const history = useHistory();
    const googleSSO = localStorage.getItem('googleSSO');

    const logoutHandler = async (e) => {
        // stop the form from submitting
        e.preventDefault();
        localStorage.removeItem("authToken");
        history.push("/login");
    } 

    return <Navbar bg="light" expand="lg">
    <Container fluid>
      <Navbar.Brand as={Link} to='/'><span><img className="navigation-login-logo" src={lykio_logo} alt=""></img></span></Navbar.Brand>
      <Navbar.Toggle aria-controls="navbarScroll" />
      <Navbar.Collapse id="navbarScroll">
        <Nav
          className="me-auto my-2 my-lg-0"
          style={{ maxHeight: '100px' }}
          navbarScroll
        >
          <Nav.Link as={Link} to='/stories'>{t('Stories')}</Nav.Link>
          <Nav.Link as={Link} to='/create'>Add Story</Nav.Link>
          <NavDropdown title="Library" id="navbarScrollingDropdown">
            <NavDropdown.Item as={Link} to="/library">Category</NavDropdown.Item>
          </NavDropdown>
          <Nav.Link as={Link} to='/leaderboards'>Leaderboards</Nav.Link>
          <Nav.Link as={Link} to='/ideology'>Ideology</Nav.Link>
          <NavDropdown title="Admin" id="navbarScrollingDropdown">
            <NavDropdown.Item as={Link} to="/reports">Reports</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/usermanagement">User Management</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/contentmanagement">Content Management</NavDropdown.Item>
          </NavDropdown>
          <NavDropdown title="Me" id="navbarScrollingDropdown">
            <NavDropdown.Item as={Link} to="/profile">Profile</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/messages">Messages</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/notifications">Notifications</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/settings">Settings</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/guides">Guides</NavDropdown.Item>
            <NavDropdown.Divider />
            {!googleSSO && <Link to="/"><span onClick={logoutHandler}>{t('Logout')}</span></Link>}
          </NavDropdown>
        </Nav>
        <Form className="d-flex">
          <FormControl
            type="search"
            placeholder="Search"
            className="me-2"
            aria-label="Search"
          />
          <Button as={Link} to='/search' variant="outline-success">Search</Button>
        </Form>
      </Navbar.Collapse>
    </Container>
  </Navbar>
}

//export default Navigation;
export default withNamespaces()(Navigation);