import { useState, useEffect } from 'react';
import axios from 'axios';
import Navigation from '../common/Navigation';
import { AUTHORIZED_URL_PREFIX, HOME_URL } from '../../../Utils/Constants';
import './home.css';
import { withNamespaces } from 'react-i18next';
import Carousel from 'react-bootstrap/Carousel';

const Home = ({ t, history }) => {
	const [ error, setError ] = useState('');
	useEffect(() => {
		const storageAuthTokenValue = localStorage.getItem('authToken');
		console.log(`PrivateScreen-useEffect: found token: ${storageAuthTokenValue}`);
		if (!storageAuthTokenValue) {
			window.history.push('/login');
		}
	}, []);
	return error ? (
		<span className="error-message">{error}</span>
	) : (
		<div>
    <Navigation></Navigation>
		<div className="under-navbar">
			<Carousel>
				<Carousel.Item>
					<img
						className="d-block w-100"
						src="https://lykio-dev-data.s3.eu-central-1.amazonaws.com/stories/2b76b64e5f177adb288a1e0d5e9d3a26"
						alt="First slide"
					/>
					<Carousel.Caption>
						<h3>First slide label</h3>
						<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<img
						className="d-block w-100"
						src="https://lykio-dev-data.s3.eu-central-1.amazonaws.com/stories/a3c69f814f7de064bc8d0654c8b391f7"
						alt="Second slide"
					/>

					<Carousel.Caption>
						<h3>Second slide label</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<img
						className="d-block w-100"
						src="https://lykio-dev-data.s3.eu-central-1.amazonaws.com/stories/a7ef37c365c13bb56fb0f276608ec508"
						alt="Third slide"
					/>

					<Carousel.Caption>
						<h3>Third slide label</h3>
						<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
					</Carousel.Caption>
				</Carousel.Item>
			</Carousel>
		</div>
	</div>
	);
};

export default withNamespaces()(Home);
