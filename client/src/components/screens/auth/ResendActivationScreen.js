import { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './resendActivationScreen.css';
import {HOME_URL ,RESENDACTIVATION_URL} from '../../../Utils/Constants';
import lykio_logo from '../../../multimedia/lykio_logo_transparent.png'; 

const ResendActivationScreen = () => {
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  const resendActivationHandler = async (e) => {
    e.preventDefault();

    const config = {
      header: {
        "Content-Type": "application/json",
      },
    };

    try {
      const { data } = await axios.post(
        RESENDACTIVATION_URL,
        { email },
        config
      );

      setSuccess(data.data);
    } catch (error) {
      setError(error.response.data.error);
      setEmail("");
      setTimeout(() => {
        setError("");
      }, 5000);
    }
  };

  return (
    <div className="resendactivation-screen">
      <form onSubmit={resendActivationHandler}
        className="resendactivation-screen__form">
        <Link to={HOME_URL} ><div><img className="login-logo" src={lykio_logo} alt=""></img></div></Link>
        <h3 className="resendactivation-screen__title">Resend Activation</h3>
        {error && <span className="error-message">{error}</span>}
        {success && <span className="success-message">{success}</span>}
        <div className="form-group">
          <p className="resendactivation-screen__subtext">
            Please enter the email address you registered your account with. An email with the account activation link will be sent to you.
          </p>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            required
            id="email"
            placeholder="Email address"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Send Email
        </button>
      </form>
    </div>
  );
};

export default ResendActivationScreen;