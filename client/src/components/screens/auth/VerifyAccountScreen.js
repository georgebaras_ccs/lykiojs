import {useState } from 'react';
import axios from 'axios';
import { Link, useParams } from 'react-router-dom';
import './verifyAccountScreen.css';
import {HOME_URL, VERIFYACCOUNT_URL} from '../../../Utils/Constants';
import lykio_logo from '../../../multimedia/lykio_logo_transparent.png'; 

const VerifyAccountScreen = () => {
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const {verificationToken} = useParams();

  const verifyAccountHandler = async (e) => {
    // stop the form from submitting
    e.preventDefault();
    // create the axios configuration
    const config = {
        header: {
            "Content-Type": "application/json"
        }
    }
    try {
        const {data} = await axios.post(VERIFYACCOUNT_URL, {verificationToken}, config);
        console.log(`VerifyAccount: made the call to ${VERIFYACCOUNT_URL}`);
        console.log(`data is ${data}`);
        setSuccess(data.data);
    } catch (error) {
        setError(error.response.data.error);
        setTimeout(()=>{
            setError("")
        }, 5000);
    }

}


return (
   <div className="verifyaccount-screen">
      
      <div>
      <form onSubmit={verifyAccountHandler} className="login-screen__form">
      <Link to={HOME_URL} ><div><img className="login-logo" src={lykio_logo} alt=""></img></div></Link>
      <h3 className="verifyaccount-screen__title">Account Verification</h3>
      <button type="submit" className="btn btn-primary">Verify my account</button>
      <div>
      {error && <span className="verifyaccount-error-message">{error}</span>}
      {success && <span className="verifyaccount-success-message">{success}</span>}
      </div>
      <Link to={HOME_URL} ><div className="verifyaccount-bottom-text">Go to the login screen</div></Link>
      </form>
      </div>
  </div>
);
};

export default VerifyAccountScreen;