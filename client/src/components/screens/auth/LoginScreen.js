import {useState, useEffect} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import lykio_logo from '../../../multimedia/lykio_logo_transparent.png';
import {HOME_URL, LOGIN_URL, LOGIN_WITH_GOOGLE, GOOGLE_CLIENT_ID} from '../../../Utils/Constants';
import './LoginScreen.css';
import GoogleLogin from 'react-google-login';

const LoginScreen = ({history}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");

    const onGoogleLoginSuccess = async(res)=>{
        // The res.profileObj contains email, name (full name), given name, family name, googleId, imageUrl
        console.log('Login Success:', res.profileObj);
        const config = {
            header: {
                "Content-Type": "application/json"
            }
        }

        try {
            const {data} = await axios.post(LOGIN_WITH_GOOGLE, 
                { firstname: res.profileObj.givenName, 
                  lastname: res.profileObj.familyName, 
                  email: res.profileObj.email, 
                  profilepicture: res.profileObj.imageUrl,
                  googleId: res.profileObj.googleId
                }, config);
            console.log(`Login: made the call to ${LOGIN_WITH_GOOGLE} and got token:${data.authToken}`);
            localStorage.setItem("authToken", data.authToken);
            localStorage.setItem("googleSSO", data.googleSSO);
            history.push(HOME_URL);
        } catch (error) {
            setError(error.response.data.error);
            setTimeout(()=>{
                setError("")
            }, 5000);
        }

    }

    const onGoogleLoginFailure = ()=>{
        setTimeout(()=>{
            setError("There was an error while trying to log in with your Google account. Please try again later")
        }, 5000);
    }

    useEffect(() => {
        const storageAuthTokenValue = localStorage.getItem("authToken");
        if (storageAuthTokenValue) {
          console.log(`Login: found token: ${storageAuthTokenValue}`);  
          history.push(HOME_URL);
        }
      }, [history]);

    const loginHandler = async (e) => {
        // stop the form from submitting
        e.preventDefault();
        // create the axios configuration
        const config = {
            header: {
                "Content-Type": "application/json"
            }
        }

        try {
            const {data} = await axios.post(LOGIN_URL, { email, password}, config);
            console.log(`Login: made the call to ${LOGIN_URL} and got token:${data.authToken}`);
            localStorage.setItem("authToken", data.authToken);
            history.push(HOME_URL);
        } catch (error) {
            setError(error.response.data.error);
            setTimeout(()=>{
                setError("")
            }, 5000);
        }

    }

    return ( <div className="login-screen">
        <form onSubmit={loginHandler} className="login-screen__form">
        <Link to={HOME_URL} ><div><img className="login-logo" src={lykio_logo} alt=""></img></div></Link>
            {error && <span className="error-message">{error}</span>}

            <div className="form-group">
                <label htmlFor="email">Email:</label>
                <input
                    type="email" required id="email" placeholder="Enter email" value={email}
                    onChange={(e)=> {setEmail(e.target.value)}} tabIndex={1}
                />
            </div>

            <div className="form-group">
                <label htmlFor="password">Password:</label>
                <input
                    type="password" required id="password" placeholder="Enter password" value={password}
                    onChange={(e)=> {setPassword(e.target.value)}} tabIndex={2}
                />
            </div>

            <button type="submit" className="btn btn-primary" tabIndex={3}>Login</button>
            <Link to="/forgotpassword"><button className="btn btn-secondary half-em-top-margin" tabIndex={4}>Forgot Password</button></Link>
            <span className="login-screen__subtext">Don't have an account? <Link to="/register">Register Here</Link></span>
            <div><span className="login-screen__subtext">Missed the activation email? <Link to="/resendactivation">Resend Activation Email</Link></span></div>
            <GoogleLogin
                    clientId={GOOGLE_CLIENT_ID}
                    buttonText="Sign In"
                    onSuccess={onGoogleLoginSuccess}
                    onFailure={onGoogleLoginFailure}
                    cookiePolicy={'single_host_origin'}
                    isSignedIn={true}
                />
        </form>
    </div> );
}

export default LoginScreen;