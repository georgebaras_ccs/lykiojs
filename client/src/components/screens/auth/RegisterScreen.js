import {useState, useEffect} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import lykio_logo from '../../../multimedia/lykio_logo_transparent.png';
import {HOME_URL, REGISTER_URL, FRONTEND_LOGIN_URL} from '../../../Utils/Constants';
import './RegisterScreen.css';

const RegisterScreen = ({history}) => {
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [error, setError] = useState("");

    useEffect(
        ()=>{
            const storageAuthTokenValue = localStorage.getItem("authToken");
            if(storageAuthTokenValue){
                history.push(FRONTEND_LOGIN_URL)
            }
        },[history]
    );

    const registerHandler = async (e) => {
        // stop the form from submitting
        e.preventDefault();
        // create the axios configuration
        const config = {
            header: {
                "Content-Type": "application/json"
            }
        }

        if(password!==confirmPassword){
            setPassword("");
            setConfirmPassword("");
            setTimeout(()=>{
                setError("")
            }, 5000);
            return setError("Passwords do not match");
        }

        try {
            const {data} = await axios.post(REGISTER_URL, {username, email, password}, config);
            if(data.success===true){
                console.log('Registration success!');
            }
            history.push(FRONTEND_LOGIN_URL);
        } catch (error) {
            setError(error.response.data.error);
            setTimeout(()=>{
                setError("")
            }, 5000);
        }

    }

    return ( <div className="register-screen">
        <form onSubmit={registerHandler} className="register-screen__form">
        <Link to={HOME_URL} ><div><img className="login-logo" src={lykio_logo} alt=""></img></div></Link>
            <h3 className="register-screen__title">Sign up for Lykio</h3>
            {error && <span className="error-message">{error}</span>}
            <div className="form-group">
                <label htmlFor="name">Username:</label>
                <input
                    type="text" required id="name" placeholder="Enter username" value={username}
                    onChange={(e)=> {setUsername(e.target.value)}}
                />
            </div>

            <div className="form-group">
                <label htmlFor="email">Email:</label>
                <input
                    type="email" required id="email" placeholder="Enter email" value={email}
                    onChange={(e)=> {setEmail(e.target.value)}}
                />
            </div>

            <div className="form-group">
                <label htmlFor="password">Password:</label>
                <input
                    type="password" required id="password" placeholder="Enter password" value={password}
                    onChange={(e)=> {setPassword(e.target.value)}}
                />
            </div>

            <div className="form-group">
                <label htmlFor="confirmpassword">Confirm password:</label>
                <input
                    type="password" required id="confirmpassword" placeholder="Retype password" value={confirmPassword}
                    onChange={(e)=> {setConfirmPassword(e.target.value)}}
                />
            </div>

            <button type="submit" className="btn btn-primary">Register</button>
            <Link to="/forgotpassword"><button className="btn btn-secondary half-em-top-margin" >Resend Activation</button></Link>
            <span className="register-screen__subtext">Already have an account? <Link to="/login">Go to Login</Link></span>
        </form>
    </div> );
}

export default RegisterScreen;