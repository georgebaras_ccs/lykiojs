import { useState, useEffect } from 'react';
import axios from 'axios';
import Navigation from '../common/Navigation';
import {AUTHORIZED_URL_PREFIX, LIBRARY_URL} from '../../../Utils/Constants';
import './library.css';
import { withNamespaces } from 'react-i18next';
import Unit from './Unit';


const Library = ({t,history}) => {
  const [error, setError] = useState("");
  const [units, setUnits] = useState([]);

  useEffect(() => {
    const storageAuthTokenValue = localStorage.getItem("authToken");
    console.log(`PrivateScreen-useEffect: found token: ${storageAuthTokenValue}`);
    if(!storageAuthTokenValue){
        window.history.push("/login")
    }

    const fetchLibraryData = async () => {
      const storageAuthTokenValue = localStorage.getItem("authToken");
      const config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${storageAuthTokenValue}`,
        },
      };

      try {
        console.log(`PrivateScreen-before call: about to make a call to /api/private with token: ${storageAuthTokenValue}`);
        const { data } = await axios.get(`${AUTHORIZED_URL_PREFIX}${LIBRARY_URL}`, config);
        setUnits(data.data);
      } catch (error) {
        localStorage.removeItem("authToken");
        setError("You are not authorized please login");
      }
    };

    fetchLibraryData();
  }, []);
  return error ? (
    <span className="error-message">{error}</span>
  ) : (
    <div>
    <Navigation></Navigation>
    <div className="under-navbar">
    {units.map(unit => {
        return <Unit s3Url={unit.s3Url} title={unit.title} coverS3Link={unit.coverS3Link}/>
    })}
    </div>
    </div>
  );
};

export default withNamespaces()(Library);