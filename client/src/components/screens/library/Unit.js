import './unit.css';
import { withNamespaces } from 'react-i18next';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

const Unit = (props)=>{
    return 	<Card style={{ width: '18rem' }}>
    <Card.Img variant="top" src={props.coverS3Link} />
    <Card.Body>
      <Card.Title>{props.title}</Card.Title>
      <Card.Text>
        Some quick example text to build on the card title and make up the bulk of the card's content.
      </Card.Text>
      <a href={props.s3Url} target="_blank" rel="noreferrer">
        <Button variant="primary">Open in new tab</Button>
      </a>
    </Card.Body>
  </Card>;
}

export default withNamespaces()(Unit);