import { useState, useEffect } from 'react';
import axios from 'axios';
import Navigation from '../common/Navigation';
import {Link} from 'react-router-dom';
import {AUTHORIZED_URL_PREFIX, CONTENT_MANAGEMENT_URL, CONTENT_MANAGEMENT_MODERATE_URL, CONTENT_MANAGEMENT_CREATE_URL} from '../../../Utils/Constants';
import './contentManagement.css';
import { withNamespaces } from 'react-i18next';


const ContentManagement = ({t,history}) => {
  const [error, setError] = useState("");
  const [contentManagement, setContentManagement] = useState("");

  useEffect(() => {
    const storageAuthTokenValue = localStorage.getItem("authToken");
    console.log(`PrivateScreen-useEffect: found token: ${storageAuthTokenValue}`);
    if(!storageAuthTokenValue){
        window.history.push("/login")
    }
  }, []);
  return error ? (
    <span className="error-message">{error}</span>
  ) : (
    <div>
    <Navigation></Navigation>
    <div className="under-navbar">
    <Link to={CONTENT_MANAGEMENT_MODERATE_URL}><button className="btn" >Manage/Moderate Units</button></Link>
    <Link to={CONTENT_MANAGEMENT_CREATE_URL}><button className="btn" >Create System Content</button></Link>    
    </div>
    </div>
  );
};

export default withNamespaces()(ContentManagement);