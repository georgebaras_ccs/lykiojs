import { useState, useEffect } from 'react';
import axios from 'axios';
import Navigation from '../common/Navigation';
// import {AUTHORIZED_URL_PREFIX, CONTENT_MANAGEMENT_URL} from '../../../Utils/Constants';
import './contentCreation.css';
import { withNamespaces } from 'react-i18next';
import grapesjs from 'grapesjs';
import gjsPresetWebpage from 'grapesjs-preset-webpage';
import Button from 'react-bootstrap/Button';

const ContentCreation = ({t,history}) => {
  const [error, setError] = useState("");
  const [editor, setEditor] = useState("");

  const saveHtmlPage = () => {
    let title = "some title"
    let unit = 
    `<!DOCTYPE html>
    <html>
    <head>
    <title>${title}</title>
    <style>
    ${editor.getCss()}
    </style>
    </head>
    <body>
    ${editor.getHtml()}
    </body>
    </html>`;
    console.log(unit);
  }

  useEffect(() => {
    const storageAuthTokenValue = localStorage.getItem("authToken");
    console.log(`PrivateScreen-useEffect: found token: ${storageAuthTokenValue}`);
    if(!storageAuthTokenValue){
        window.history.push("/login")
    }

    const editor = grapesjs.init({
      container: "#editor",
      plugins: [gjsPresetWebpage],
      pluginsOpts:{
        gjsPresetWebpage:{}
      }
    });
    setEditor(editor);


  }, []);
  return error ? (
    <span className="error-message">{error}</span>
  ) : (
    <div>
    <Navigation></Navigation>
    <div> <Button onClick={saveHtmlPage}>Save Unit</Button> </div>
    <div className="under-navbar" id="editor"></div>
    </div>
  );
};

export default withNamespaces()(ContentCreation);