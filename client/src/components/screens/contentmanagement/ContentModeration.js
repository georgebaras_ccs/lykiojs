import { useState, useEffect } from 'react';
import Navigation from '../common/Navigation';
import './contentModeration.css';
import { withNamespaces } from 'react-i18next';


const ContentModeration = ({t,history}) => {
  const [error, setError] = useState("");
  useEffect(() => {
    const storageAuthTokenValue = localStorage.getItem("authToken");
    console.log(`PrivateScreen-useEffect: found token: ${storageAuthTokenValue}`);
    if(!storageAuthTokenValue){
        window.history.push("/login")
    }
  }, []);
  return error ? (
    <span className="error-message">{error}</span>
  ) : (
    <div>
    <Navigation></Navigation>
    <div className="under-navbar">Content moderation here</div>
    </div>
  );
};

export default withNamespaces()(ContentModeration);