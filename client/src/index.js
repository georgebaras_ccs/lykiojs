import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'grapesjs/dist/css/grapes.min.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './i18n';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
