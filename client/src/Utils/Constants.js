export const BACKEND_BASE_URL = "http://localhost:5000";
export const GOOGLE_CLIENT_ID = "1017808396535-1k903c5vnpeqp1b1545024t6jrgje5d5.apps.googleusercontent.com"
export const HOME_URL = "/";
export const AUTH_TOKEN_NAME = "authToken";
export const FRONTEND_REGISTER_URL = "/register";
export const FRONTEND_LOGIN_URL = "/login";
export const FRONTEND_FORGOTPASS_URL = "/forgotpassword";
export const FRONTEND_RESENDACTIVATION_URL = "/resendactivation";
export const FRONTEND_PASSWORDRESET_URL = "/passwordreset/:resetToken";
export const FRONTEND_VERIFYACCOUNT_URL = "/verifyaccount/:verificationToken";
export const LOGIN_URL = "/api/auth/login";
export const REGISTER_URL = "/api/auth/register";
export const LOGIN_WITH_GOOGLE = "/api/auth/googlelogin"
export const FORGOTPASS_URL = "/api/auth/forgotpassword";
export const RESETPASS_URL = "/api/auth/resetpassword";
export const RESENDACTIVATION_URL = "/api/auth/resendactivation";
export const VERIFYACCOUNT_URL = "/api/auth/verifyaccount";
export const AUTHORIZED_URL_PREFIX = "/api/private";
export const STORIES_URL = "/stories";
export const CONTENT_MANAGEMENT_URL = "/contentmanagement";
export const CONTENT_MANAGEMENT_CREATE_URL = "/create-new-content";
export const CONTENT_MANAGEMENT_MODERATE_URL = "/manage-content";
export const USER_MANAGEMENT_URL = "/usermanagement";
export const CREATE_URL = "/create";
export const GUIDES_URL = "/guides";
export const IDEOLOGY_URL = "/ideology";
export const SETTINGS_URL = "/settings";
export const SEARCH_URL = "/search";
export const REPORTS_URL = "/reports";
export const PROFILE_URL = "/profile";
export const NOTIFICATIONS_URL = "/notifications";
export const MESSAGES_URL = "/messages";
export const LIBRARY_URL = "/library";
export const LEADERBOARDS_URL = "/leaderboards";





