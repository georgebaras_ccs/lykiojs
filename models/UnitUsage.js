const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UnitUsageSchema = new mongoose.Schema({
    creationDate: Date
});

UnitUsageSchema.pre("save", async function(next){
  this.creationDate = new Date();
  next();
});

const UnitUsage = mongoose.model("UnitUsage", UnitUsageSchema);

module.exports = UnitUsage;