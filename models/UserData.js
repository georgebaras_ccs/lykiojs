const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserDataSchema = new mongoose.Schema({
    creationDate: Date
});

UserDataSchema.pre("save", async function(next){
  this.creationDate = new Date();
  next();
});

const UserData = mongoose.model("UserData", UserDataSchema);

module.exports = UserData;