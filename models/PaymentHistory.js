const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PaymentHistorySchema = new mongoose.Schema({
    creationDate: Date
});

PaymentHistorySchema.pre("save", async function(next){
  this.creationDate = new Date();
  next();
});

const PaymentHistory = mongoose.model("PaymentHistory", PaymentHistorySchema);

module.exports = PaymentHistory;