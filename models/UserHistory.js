const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserHistorySchema = new mongoose.Schema({
    creationDate: Date
});

UserHistorySchema.pre("save", async function(next){
  this.creationDate = new Date();
  next();
});

const UserHistory = mongoose.model("UserHistory", UserHistorySchema);

module.exports = UserHistory;