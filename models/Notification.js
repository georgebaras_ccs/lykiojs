const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationSchema = new mongoose.Schema({
    creationDate: Date
});

NotificationSchema.pre("save", async function(next){
  this.creationDate = new Date();
  next();
});

const Notification = mongoose.model("Notification", NotificationSchema);

module.exports = Notification;