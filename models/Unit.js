const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UnitSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, "Please provide a title"]
    }, 
    defaultLanguage: {
        type: String,
        default: 'en'
    },
    availableLanguages: 
        [
            {
                lang:{type: String}, 
                s3Url: {type: String},
                title: {type: String},
                description: {type: String}
            }
        ]
    ,
    description: {
        type: String
    },
    type: {
        type: String
    },
    coverS3Link: {
        type: String
    },
    mediatype: {
        type: String
    },
    s3Url: {
        type: String
    },
    originalFileName: {
        type: String
    },
    size: {
        type: Number
    },
    status: {
      type : String,
      default: 'pending'
    },
    uploader: {
        type: Schema.Types.ObjectId, 
        ref: 'User'
    },
    approver: {
        type: Schema.Types.ObjectId, 
        ref: 'User'
    },
    ratings: 
        [
            {
            rating:{type: Number}, 
            rater: {type: Schema.Types.ObjectId}
            }
        ]
    ,
    comments: 
        [
            {
            comment:{type: String}, 
            commenterId: {type: Schema.Types.ObjectId},
            commenterName: {type: String}
            }
        ]
    ,
    creationDate: Date,
    updateDate: Date
});

UnitSchema.pre("save", async function(next){
  this.creationDate = new Date();
  this.updateDate = this.creationDate;
  next();
});

const Unit = mongoose.model("Unit", UnitSchema);

module.exports = Unit;