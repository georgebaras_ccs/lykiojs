#!/bin/bash
echo "Going into lykiojs folder"
cd /home/ubuntu/lykiojs/
echo "** killing old server"
sudo pm2 delete all
echo "** Starting LykioJS"
sudo pm2 start server.js
