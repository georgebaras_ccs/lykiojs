#!/bin/bash
if [ $# -eq 0 ]; then
    echo "No branch provided, e.g. main. The deployment will use the main branch"
    BRANCH="main"
fi
BRANCH="$1" # eg develop
# go to lykiojs project
cd /home/ubuntu/lykiojs/
echo "** pulling new code" 
git pull
echo "** checking out the requested branch"
git checkout $BRANCH
echo "** npm install on server"
sudo npm install
echo "** going into the client"
cd /home/ubuntu/lykiojs/client/
echo "** npm install on react client"
sudo npm install
echo "** building the React app"
sudo npm run build
echo "back to lykiojs folder"
cd /home/ubuntu/lykiojs/
echo "** killing old server"
sudo pm2 delete all
echo "** Starting LykioJS"
sudo pm2 start server.js
