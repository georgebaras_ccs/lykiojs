const express = require('express');
const router = express.Router();

const {register, login, googlelogin, resetpassword, forgotpassword, verifyaccount, resendactivation} = require('../controllers/auth');

router.route("/register").post(register);

router.route("/login").post(login);

router.route("/googlelogin").post(googlelogin);

router.route("/forgotpassword").post(forgotpassword);

router.route("/resetpassword").post(resetpassword);

router.route("/verifyaccount").post(verifyaccount);

router.route("/resendactivation").post(resendactivation);

module.exports = router;