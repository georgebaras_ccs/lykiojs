require('dotenv').config({ path: './config.env' });
const express = require('express');
const app = express();
const connectDB = require('./config/db');
const errorHandler = require('./middleware/error');
const path = require('path');
const User = require('./models/User');
const {
	HOMEURL,
	STORIES_URL,
	LIBRARY_URL,
	LEADERBOARDS_URL,
	SEARCH_URL,
	NOTIFICATIONS_URL,
	MESSAGES_URL,
	CREATE_URL,
	PROFILE_URL,
	IDEOLOGY_URL,
	SETTINGS_URL,
	GUIDES_URL,
	REPORTS_URL,
	USERMANAGEMENT_URL,
	CONTENTMANAGEMENT_URL,
	CONTENTMODERATION_URL,
	CONTENTCREATION_URL
} = require('./utils/constants');

connectDB();

app.use(express.json());

// Connecting Routes
app.use('/api/auth', require('./routes/auth'));
app.use('/api/private', require('./routes/private'));

// Error Handler Middleware
app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(PORT, () => console.log(`Lykio running on port ${PORT}`));
('/');

process.on('unhandledRejection', (err, promise) => {
	console.log(`Logged Error: ${err.message}`);
	server.close(() => process.exit(1));
});

// Make the react build public
app.use(express.static(path.join(__dirname, './client/build')));
// Mirror all the calls to avoid having dead url errors
app.get(
	[ "register",
		HOMEURL,
		STORIES_URL,
		LIBRARY_URL,
		LEADERBOARDS_URL,
		SEARCH_URL,
		NOTIFICATIONS_URL,
		MESSAGES_URL,
		CREATE_URL,
		PROFILE_URL,
		IDEOLOGY_URL,
		SETTINGS_URL,
		GUIDES_URL,
		REPORTS_URL,
		USERMANAGEMENT_URL,
		CONTENTMANAGEMENT_URL,
		CONTENTMODERATION_URL,
		CONTENTCREATION_URL
	],
	(req, res) => {
		res.sendFile(path.join(__dirname, './client/build', 'index.html'));
	}
);
