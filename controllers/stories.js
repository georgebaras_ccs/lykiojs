const Unit = require('../models/Unit');

exports.getStories = async (req, res, next) => {
    const stories = await Unit.find({type: 'story'});
    const storiesDTO = [];
    stories.forEach(story => {
        storiesDTO.push({s3Url: story.s3Url, title: story.title});
    });
    res.status(200).json({
         success: true,
         data: storiesDTO
    });
}