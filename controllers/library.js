const Unit = require('../models/Unit');

exports.getLibrary = async (req, res, next) => {
    const stories = await Unit.find({type: 'system'});
    const storiesDTO = [];
    stories.forEach(unit => {
        storiesDTO.push({s3Url: unit.s3Url, title: unit.title, coverS3Link: unit.coverS3Link});
    });
    res.status(200).json({
         success: true,
         data: storiesDTO
    });
}