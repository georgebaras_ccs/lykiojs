const Unit = require('../models/Unit');
const User = require('../models/User');
const {uploadFileToS3} = require('../utils/s3');

exports.createStory = async (req, res, next) => {
    const file = req.file;
    const filePath = file.path;
    await uploadFileToS3(file);
    const s3Url = `${process.env.AWS_BUCKET_BASE_URL}${process.env.AWS_BUCKET_STORIES_PREFIX}${file.filename}`;
    // ^ the file will be uploaded to S3 and then the S3 link will be stored instead of the filepath
    const title = req.body.title;
    try {
        const unit = await Unit.create({
            title: title,
            s3Url: s3Url,
            coverS3Link: s3Url,
            mediatype:  file.mimetype,
            originalFileName: file.originalname,
            size: file.size,
            status : "pending",
            type: "story",
            uploader: req.user._id,
            availableLanguages: [{lang: "es", s3Url: filePath},{lang: "el", s3Url: filePath}],
            ratings: [{rating: 5, rater: req.user._id},{rating: 4, rater: req.user._id}],
            comments: [{comment: "Hey there", commenterId: req.user._id, commenterName: req.user.username},{comment: "Hey there back!", commenterId: req.user._id, commenterName: req.user.username}]
        });
        // Also add the story to the user's stories
        const userToUpdate = await User.findOne({
            _id : req.user._id
        });
        userToUpdate.stories ? userToUpdate.stories.push(unit._id) : userToUpdate.stories=[unit._id];
        await userToUpdate.save();

        res.status(201).json({
            success: true,
            data: `You have created a story with title: ${title} and db id: ${unit._id} from controllers/create.`
        });
    } catch (err) {
        next(err);
    }
}