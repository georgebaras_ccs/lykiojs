exports.getHome = (req, res, next) => {
    res.status(200).json({
        success: true,
        data: "Home data from controllers/home"
    });
}