exports.getSettings = (req, res, next) => {
    res.status(200).json({
        success: true,
        data: "Settings data from controllers/settings"
    });
}