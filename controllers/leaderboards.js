exports.getLeaderboards = (req, res, next) => {
    res.status(200).json({
        success: true,
        data: "Leaderboards data from controllers/leaderboards"
    });
}