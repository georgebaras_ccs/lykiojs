exports.DEFAULT_PROFILE_PICTURE = "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png";
exports.PENDING_USER = "pending";
exports.ACTIVE_USER = "active";
exports.LOCKED_USER = "locked";
exports.PENDING_UNIT= "pending";
exports.APPROVED_UNIT = "approved";

// All urls mirrored with the frontend
exports.HOMEURL = "/";
exports.STORIES_URL = "/stories";
exports.LIBRARY_URL = "/library";
exports.LEADERBOARDS_URL = "/leaderboards";
exports.SEARCH_URL = "/search";
exports.NOTIFICATIONS_URL = "/notifications";
exports.MESSAGES_URL = "/messages";
exports.CREATE_URL = "/create";
exports.PROFILE_URL = "/profile";
exports.IDEOLOGY_URL = "/ideology";
exports.SETTINGS_URL = "/settings";
exports.GUIDES_URL = "/guides";
exports.REPORTS_URL = "/reports";
exports.USERMANAGEMENT_URL = "/usermanagement";
exports.CONTENTMANAGEMENT_URL = "/contentmanagement";
exports.CONTENTMODERATION_URL = "/manage-content";
exports.CONTENTCREATION_URL = "/create-new-content";
